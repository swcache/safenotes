(function() {

    function enableStandardSW(btn) {
        btn.innerText = 'Enable protected SW';
        btn.classList.remove('is-warning');
        btn.classList.add('is-success');
        navigator.serviceWorker.register('/safenotes/sw.js')
            .then(reg => console.log('Standard ServiceWorker registrated successfully with scope: ', reg.scope))
            .catch(err => console.log('Standard ServiceWorker registration failed: ', err));
    }

    function enableProtectedSW(btn) {
        btn.innerText = 'Enable standard SW';
        btn.classList.remove('is-success');
        btn.classList.add('is-warning');
        navigator.serviceWorker.register('/safenotes/sw-mitigated.js')
            .then(reg => console.log('Protected ServiceWorker registrated successfully with scope: ', reg.scope))
            .catch(err => console.log('Protected ServiceWorker registration failed: ', err));
    }

    let protected = localStorage.getItem('sw-protected');
    let protectionBtn = document.querySelector('#btn-protection');

    if(protected === 'enabled') {
        enableProtectedSW(protectionBtn);
    } else {
        enableStandardSW(protectionBtn);
        localStorage.setItem('sw-protected', 'disabled');
    }

    protectionBtn.addEventListener('click', (event) => {
        protectedPrev = localStorage.getItem('sw-protected');
        if(protectedPrev === 'enabled') {
            enableStandardSW(protectionBtn);
            localStorage.setItem('sw-protected', 'disabled');
        } else {
            enableProtectedSW(protectionBtn);
            localStorage.setItem('sw-protected', 'enabled');
        }
    });
})();