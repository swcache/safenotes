self.addEventListener('install', (event) => {
    self.skipWaiting();
  
    event.waitUntil(
        caches.open('static').then((cache) => cache.addAll([
            '/safenotes/',
            '/safenotes/safenotes.js'
        ]))
    );
});

self.addEventListener('fetch', (event) => {
    event.respondWith(async function() { 
        let cResponse = await caches.match(event.request);
        if(cResponse) return cResponse;
        let response = await fetch(event.request);
        let responseClone = response.clone();
        let cache = await caches.open('static');
        cache.put(event.request, responseClone);
        return response;
    }());
});