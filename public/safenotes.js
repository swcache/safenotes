(function() {
    // crypto code adapted from https://github.com/bradyjoslin/webcrypto-example/blob/master/script.js
    const buff_to_base64 = (buff) => btoa(String.fromCharCode.apply(null, buff));
    const base64_to_buf = (b64) => Uint8Array.from(atob(b64), (c) => c.charCodeAt(null));

    const enc = new TextEncoder();
    const dec = new TextDecoder();

    async function encrypt() {
        const data = window.document.getElementById("data").value;
        let encryptedDataOut = window.document.getElementById("encryptedData");
        const password = window.prompt("Password");
        const encryptedData = await encryptData(data, password);
        encryptedDataOut.value = encryptedData;
    }

    async function decrypt() {
        const password = window.prompt("Password");
        const encryptedData = window.document.getElementById("encryptedData").value;
        let decryptedDataOut = window.document.getElementById("decrypted");
        const decryptedData = await decryptData(encryptedData, password);
        decryptedDataOut.value = decryptedData || "decryption failed!";
    }

    const getPasswordKey = (password) =>
        window.crypto.subtle.importKey("raw", enc.encode(password), "PBKDF2", false, [
            "deriveKey",
        ]);

    const deriveKey = (passwordKey, salt, keyUsage) =>
        window.crypto.subtle.deriveKey(
            {
                name: "PBKDF2",
                salt: salt,
                iterations: 250000,
                hash: "SHA-256",
            },
            passwordKey,
            { name: "AES-GCM", length: 256 },
            false, // non extractable
            keyUsage
        );

    async function encryptData(secretData, password) {
        try {
            const salt = window.crypto.getRandomValues(new Uint8Array(16));
            const iv = window.crypto.getRandomValues(new Uint8Array(12));
            const passwordKey = await getPasswordKey(password);
            const aesKey = await deriveKey(passwordKey, salt, ['encrypt']);
            const encryptedContent = await window.crypto.subtle.encrypt(
                {
                    name: 'AES-GCM',
                    iv: iv,
                },
                aesKey,
                enc.encode(secretData)
            );
            const encryptedContentArr = new Uint8Array(encryptedContent);
            let buff = new Uint8Array(
                salt.byteLength + iv.byteLength + encryptedContentArr.byteLength
            );
            buff.set(salt, 0);
            buff.set(iv, salt.byteLength);
            buff.set(encryptedContentArr, salt.byteLength + iv.byteLength);
            const base64Buff = buff_to_base64(buff);

            return base64Buff;
        } catch (e) {
            console.log(`Error - ${e}`);
            return '';
        }
    }

    async function decryptData(encryptedData, password) {
        try {
            const encryptedDataBuff = base64_to_buf(encryptedData);
            const salt = encryptedDataBuff.slice(0, 16);
            const iv = encryptedDataBuff.slice(16, 16 + 12);
            const data = encryptedDataBuff.slice(16 + 12);
            const passwordKey = await getPasswordKey(password);
            const aesKey = await deriveKey(passwordKey, salt, ["decrypt"]);
            const decryptedContent = await window.crypto.subtle.decrypt(
                {
                    name: "AES-GCM",
                    iv: iv,
                },
                aesKey,
                data
            );
            return dec.decode(decryptedContent);
        } catch (e) {
            console.log(`Error - ${e}`);
            return '';
        }
    }

    async function setNote(name, data, password,) {
        let eData = await encryptData(data, password);
        localStorage.setItem(name, eData);
    }

    async function getNote(name, password) {
        let data = await decryptData(localStorage.getItem(name), password);
        return data || 'Decryption failed...';
    }

    function renderNote(name, data) {
        // clear
        document.querySelector('#col-notes').innerHTML = '';
        // build note
        let colNotes = document.querySelector('#col-notes');
        let divNode = document.createElement('div');
        divNode.className = 'box';
        let hNode = document.createElement('h3');
        hNode.className = 'title is-3';
        hNode.appendChild(document.createTextNode(name));
        let pNode = document.createElement('p');
        pNode.appendChild(document.createTextNode(data));
        // append note
        divNode.appendChild(hNode);
        divNode.appendChild(pNode);
        colNotes.appendChild(divNode);
    }   


    // reset localstorage and clear notes
    document.querySelector('#btn-reset').addEventListener('click', (event) => {
        localStorage.clear();
        document.querySelector('#col-notes').innerHTML = '';
    });

    // set note in the localstorage
    document.querySelector('#col-add button').addEventListener('click', (event) => {
        let noteDataNode = document.querySelector('#col-add textarea');
        let noteNameNode = document.querySelector('#col-add input[type="text"]');
        let passwordNode = document.querySelector('#col-add input[type="password"]');
        let noteData = noteDataNode.value;
        let noteName = noteNameNode.value;
        let password = passwordNode.value;

        document.querySelector('#col-add form').reset();
        setNote(noteName, noteData, password);
    });

    // get a note from localstorage and render it
    document.querySelector('#col-display button').addEventListener('click', async (event) => {
        let noteName = document.querySelector('#col-display input[type="text"]').value;
        let password = document.querySelector('#col-display input[type="password"]').value;
        
        document.querySelector('#col-display form').reset();

        let noteData = await getNote(noteName, password);
        if(noteData !== undefined && noteData !== null && noteData.length > 0) {
            renderNote(noteName, noteData);
        }
    });
})();